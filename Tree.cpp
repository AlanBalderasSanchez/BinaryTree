#include "Tree.h"

using namespace std;
Tree::Tree() {
	
}

Tree::~Tree() {
	
}

void Tree::insertNode(int value){
	
	Node *aux = new Node(NULL,NULL,value);
	//caso base
	if(root == NULL){
		this->root = aux;
	}	
	
	else insertRecursive(aux,root);
}

void Tree::insertRecursive(Node *newNode, Node *ref){
	
	//Caso inserción izquierda
	if(newNode->value < ref->value){
		
		//Si tiene hijos en la izquierda, llamar a recursivo con el hijo izquierdo
		if(ref->left != NULL)
			insertRecursive(newNode,ref->left);
		
		else ref->left = newNode;
		
	}
	
	else if(newNode->value > ref->value){
		
		//Si tiene hijos en la izquierda, llamar a recursivo con el hijo izquierdo
		if(ref->right != NULL)
			insertRecursive(newNode,ref->right);
		
		else ref->right = newNode;
		
	}
	
}

void Tree::inOrder(){
	this->inOrderTransversal(root);
}
	
void Tree::inOrderTransversal(Node *node){
	
	if(node!=NULL){
		inOrderTransversal(node->left);
		cout << node->value << ",";
		inOrderTransversal(node->right);
	}
}

void Tree::preOrder(){
	
	this->preOrderTransversal(root);
}

void Tree::preOrderTransversal(Node *node){
	if(node!=NULL){
		cout << node->value << ",";
		preOrderTransversal(node->left);
		preOrderTransversal(node->right);
	}
}

void Tree::postOrder(){
	this->postOrderTransversal(root);
}

void Tree::postOrderTransversal(Node *node){
	if(node!=NULL){
		postOrderTransversal(node->left);
		postOrderTransversal(node->right);
		cout << node->value << ",";
	}
}
