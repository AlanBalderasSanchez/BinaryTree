#ifndef TREE_H
#define TREE_H

#include <iostream>
#include "Node.h"

using namespace std;
class Tree {
private:
	int getHeight();
	void inOrderTransversal(Node*);
	void postOrderTransversal(Node*);
	void preOrderTransversal(Node*);
protected:
	
public:
	Tree();
	~Tree();
	Node *root = NULL;
	void insertNode(int value);
	void insertRecursive(Node *,Node *);
	void deleteNode(int value);
	void inOrder();
	void postOrder();
	void preOrder();
	void balance_factor();
	void height();
};

#endif

