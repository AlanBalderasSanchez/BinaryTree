#ifndef NODE_H
#define NODE_H

using namespace std;
class Node {
private:
protected:
public:
	Node(Node *left,Node *right,int value);
	~Node();
	Node *left,*right;
	int value;
	int getValue();
};

#endif

