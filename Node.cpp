#include "Node.h"

using namespace std;
Node::Node(Node *left,Node *right,int value) {
	this->value = value;
	this->left  = left;
	this->right = right;
}

Node::~Node() {
	
}

int Node::getValue(){	
	return this->value;
}

